<?php

namespace KeiCreations\ReferenceBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class ReferenceUpdateCommand extends Command
{
    protected static $defaultName = 'kei:reference';

    protected function configure()
    {
        $this->setName(self::$defaultName);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $fileName = 'composer.lock';
        $cmd = ['git', 'rev-parse', 'HEAD'];
        $name = 'keicreations/';

        if (!file_exists($fileName)) {
            throw new FileNotFoundException();
        }

        $file = json_decode(file_get_contents($fileName), true);

        foreach ($file['packages'] as $key => $value) {
            if (stripos($value['name'], $name) === 0) {
                $process = new Process($cmd);
                $process->setWorkingDirectory('vendor/' . $value['name']);
                $process->run();

                if ($process->isSuccessful()) {
                    $newValue = preg_replace('/\s+/', '', $process->getOutput());
                    if ($newValue !== $file['packages'][$key]['source']['reference']) {
                        $file['packages'][$key]['source']['reference'] = $newValue;
                        $output->writeln(
                            'Patching '.$value['name'].' to revision '.$file['packages'][$key]['source']['reference']
                        );
                        $newFile = json_encode(
                            $file,
                            JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE
                        );
                        file_put_contents('composer.lock', $newFile);
                    }
                }
                else {
                    $output->writeln(sprintf('Could not find revision for package %s', $value['name']));
                }
            }
        }
        return 0;
    }
}
